import styled from "styled-components"
import Footer from "../components/Footer"
import Navbar from "../components/Navbar"
import { LocalShipping, Remove, Add } from "@material-ui/icons"
import { Newsletter } from "../components/Newsletter"

const Container = styled.div``
const Wrapper = styled.div`
    padding: 50px;
    display: Flex;
`
const ImageContainer = styled.div`
    flex: 1;
`
const Image = styled.img`
    flex: 1;
    width: 100%;
    height: 90vh;
    object-fit: cover;
    margin-right: 50px;
`
const InfoContainer = styled.div`
    flex: 1;
    padding: 0 50px;
`
const Title = styled.h1`
    flex: 1;
    font-weight: 500;
`
const Details = styled.p`
    flex: 1;
    margin: 20px 0px;
`
const Price = styled.span`
    flex: 1;   
    font-weight: 300;
    font-size: 40px; 
`
const Icon = styled.span`
    flex: 1;
    margin: 0px 5px;
    margin-botton: 5px;
`
const Text = styled.span`
    display: inline-block;
    flex: 1;
    margin: 0px 5px;
`
const AddInfo = styled.div`
    width: 50%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`
const SymbolContainer = styled.div`
    display: flex;
    align-items: center;
    font-weight: 700;
`
const SubTotal = styled.span`
    width: 30px;
    height: 30px;
    border-radius: 10px;
    border: 1px solid;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0px 5px;
`
const Button = styled.button`
    padding: 15px;
    border: 1px solid black;
    background-color: white;
    cursor: pointer;
    font-weight: 700;

    &:hover {
        background-color: #D3E3E2;
    }
`

const Product = () => {
    return (
        <Container>
            <Navbar/>
            <Wrapper>
                <ImageContainer>
                    <Image src='https://images.pexels.com/photos/8365688/pexels-photo-8365688.jpeg?auto=compress&cs=tinysrgb&h=650&w=940'/>
                </ImageContainer>
                <InfoContainer>
                    <Title>Prada Women Cream Bag Re-edition  2000 Mini Shoulder Bag</Title>
                    <Details>Re-edition 2000 mini shoulder bag. 100%recycled polyamide, trim: bovine leather. Color: cream</Details>
                    <Price>Price: $788</Price>
                    <Icon>
                    <LocalShipping/>
                    <Text>Free shipping</Text>
                    </Icon>

                    <AddInfo>
                    <SymbolContainer>
                        <Remove/>
                        <SubTotal>1</SubTotal>
                        <Add/>
                    </SymbolContainer>
                    <Button>ADD TO CART</Button>
                </AddInfo>
                </InfoContainer>
            </Wrapper>
            <Newsletter/>
            <Footer/>
        </Container>
    )
}

export default Product
