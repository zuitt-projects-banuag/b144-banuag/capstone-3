import { Add, Remove } from '@material-ui/icons'
import React from 'react'
import styled from "styled-components"
import Footer from "../components/Footer"
import Navbar from "../components/Navbar"
import Product from '../components/Product'

const Container = styled.div``

const Wrapper = styled.div`
    padding: 20px;
`

const Title = styled.h1`
    font-weight: 300;
    text-align: center;
`
const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 20px;
`
const TopButton = styled.button`
    padding: 10px;
    font-weight: 300;
    cursor: pointer;
    border: ${props=>props.type === 'filled' && 'none'};
    background-color: ${props=>props.type === 'filled' ? 'black' : 'transparent'};
    color: ${props=>props.type === 'filled' && 'white'};
`
const TopTexts = styled.div`
    
`
const TopText = styled.span`
   text-decoration: underline;
   cursor: pointer;
   margin: 0px 10px;
`

const Bottom = styled.div`
    display: flex;
    justify-content: space-between;
`
const Info = styled.div`
    flex: 3;
`
const Products = styled.div`
    display: flex;
    justify-content: space-between;

`
const ProductInfo = styled.div`
    flex: 2;
    display: flex;
`

const Image = styled.img`
    width: 200px;
`
const ProductDetails = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    // justify-content: space-around;
`
const Name = styled.span`
    
`
const Id = styled.span`
    
`
const PriceDetails = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

`
const IconContainer = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;
`
const ProductQuantity = styled.div`
    font-size: 24px;
    margin: 5px;
`
const ItemPrice = styled.div`
    font-size: 24px;
    font-weight: 200;
`

const Summary = styled.div`
    flex: 1;
    border: 0.5px solid;
    border-radius: 10px;
    padding: 20px;
    // height: 50vh;
`
const SummaryTitle = styled.h1`
    font-weight: 200;
`
const SummaryItem = styled.div`
    margin: 30px 0px;
    display: flex;
    justify-content: space-between;
    font-weight: ${props=>props.type === 'total' && '500'};
    font-size: ${props=>props.type === 'total' && '24px'};
`
const SummaryItemText = styled.span`
    flex: 1;
`
const SummaryItemPrice = styled.span`
    flex: 1;
`
const Button = styled.button`
    width: 100%;
    padding: 10px;
    font-weight: 500;
`

function Cart() {
    return (
        <Container>
            <Navbar/>
            <Wrapper>
                <Title>YOUR CART</Title>
                <Top>
                    <TopButton>CONTINUE SHOPPING</TopButton>
                    <TopTexts>
                        <TopText>Cart(2)</TopText>
                        <TopText>Your Wishlist(0)</TopText>
                    </TopTexts>
                    <TopButton type='filled'>CHECKOUT</TopButton>
                </Top>
                <Bottom>
                    <Info>
                        <Products>
                            <ProductInfo>
                                <Image src ='https://images.pexels.com/photos/3908800/pexels-photo-3908800.jpeg?auto=compress&cs=tinysrgb&h=650&w=940'/>
                                <ProductDetails>
                                    <Name><b>Product Name:</b> Sling Bag</Name>
                                    <Id><b>ID:</b> B0001</Id>
                                </ProductDetails>

                                <PriceDetails>
                                    <IconContainer>
                                        <Add/>
                                        <ProductQuantity>2</ProductQuantity>
                                        <Remove/>
                                    </IconContainer>
                                    <ItemPrice>$80</ItemPrice>
                                </PriceDetails>
                            </ProductInfo>
                        </Products>
                    </Info>
                    <Summary>
                        <SummaryTitle>Order Summary</SummaryTitle>
                        <SummaryItem>
                            <SummaryItemText>Subtotal</SummaryItemText>
                            <SummaryItemText>$ 80</SummaryItemText>
                        </SummaryItem>
                        <SummaryItem type='total'>
                            <SummaryItemText>Total</SummaryItemText>
                            <SummaryItemText>$ 80</SummaryItemText>
                        </SummaryItem>
                        <Button>CHECKOUT NOW</Button>
                    </Summary>
                </Bottom>
            </Wrapper>
            <Footer/>
        </Container>
    )
}

export default Cart
