import styled from 'styled-components'
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: url('https://images.pexels.com/photos/4173108/pexels-photo-4173108.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    padding: 20px;
    background-color: white;
    margin-right: 40%;
`

const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`

const Form = styled.form`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
`

const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 10px 0px 0px;
    padding: 10px;
`

const Text = styled.p`
    padding: 5px;
    font-size: 12px;
    margin: 20px 0px;
    
`

const Button = styled.button`
    width: 30%;
    height: 45px;
    margin-top: 20px;
    border: none;
    padding: 15px 20px;
    background-color: #D3E3E2; 
    cursor: pointer;
`
const Register = () => {

    return (
		
        <Container>
            <Wrapper>
                <Title>CREATE AN ACCOUNT</Title>
                <Form>
                    <Input  
						placeholder='Enter First name'
						type="text"
						required 
					/>
                    <Input
						placeholder='Enter Last name'
						type="text"
						required 
					/>
                    <Input 
						placeholder='Age'
						type="text"
						required
					/>
                    <Input
						placeholder='Gender'
						type="text"
						required
					/>
					<Input
						placeholder='Mobile'
						type="text"
						required
					
					/>
                    <Input 
						placeholder='Email'
						type="text"
						required
					/>
                    <Input 
						placeholder='Password'
						type="password"
						required 
					/>
                    <Input 
						placeholder='Confirm Password'
						type="password"
						required
					/>
						<Button type="submit" id="submitBtn">
							SIGN UP
						</Button>
			  	
                    <p>Already have an account? <a href='/login'>Click here</a> to login</p>
                </Form>
            </Wrapper>
        </Container>
    )
}

export default Register
