import styled from "styled-components"
import Footer from "../components/Footer"
import Navbar from "../components/Navbar"
import { Newsletter } from "../components/Newsletter"
import Products from "../components/Products"

const Container = styled.div`
`
const Title = styled.h1`
    margin: 20px;
`
const ItemContainer = styled.div`
    display: flex;
`
const Filter = styled.div`
    margin: 20px;
`
const FilterText = styled.span`
    font-size: 20px;
    font weight: 600;
`
const Select = styled.select`
`

const Option = styled.option`
`

const ProductList = () => {
    return (
        <Container>

            <Navbar/>
            <Title>All Products</Title>
            <ItemContainer>
                <Filter>
                    <FilterText>Sort Products:</FilterText>
                    <Select>
                        <Option disabled selected>
                            Size
                        </Option>
                        <Option>Xxs</Option>
                        <Option>xs</Option>
                        <Option>S</Option>
                        <Option>M</Option>
                        <Option>L</Option>
                        <Option>XL</Option>
                    </Select>
                    </Filter>
            </ItemContainer>
            <Products/>
            <Newsletter/>
            <Footer/>
        </Container>
    )
}

export default ProductList
