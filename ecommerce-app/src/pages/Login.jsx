import React from 'react'
import styled from 'styled-components'
import { useState, useEffect, useContext } from 'react';
// import { Navigate } from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: url('https://images.pexels.com/photos/4173108/pexels-photo-4173108.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    padding: 20px;
    background-color: white;
    margin-right: 40%; 
`

const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`

const Form = styled.form`
    display: flex;
    flex-direction: column;
`

const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 0px;
    padding: 10px;
`
const Button = styled.button`
    width: 20%;
    height: 40px;
    margin-top: 5px;
    border: none;
    //padding: 15px 15px;
    background-color: #D3E3E2; 
    cursor: pointer;
    margin-bottom: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
`

const Link = styled.a`
    margin: 5px 0px;
    font-size: 12px;
    text-decoration: underline;
    cursor: pointer;
    text-align: center;
`
function Login() {
        return (
            <Container>
                <Wrapper>
                    <Title>SIGN IN</Title>
                    <Form>
                   <Input
                       type="email" 
                       placeholder="Enter email" 
                       required
                   />
                   <Input
                       type="password" 
                       placeholder="Password"
                       required
                   />
                   <Link>Forgot Password?</Link>
                   <Button variant="primary" type="submit" id="submitBtn">
                       Submit
                   </Button>
                   <p>Don't have an account yet? <a href='/register'>Click here</a> to register.</p>
              
            </Form> 
                </Wrapper>
            </Container>
        )
}

export default Login
