export const sliderItems = [
    {
        id: 1,
        img: 'https://i.pinimg.com/736x/84/ba/64/84ba64190cafb91bad0314858dadd44f.jpg',
        title: 'SUMMER SALE',
        desc: 'GET 30% OFF FOR NEW ARRIVALS ',
        bg: 'f5fafd',
    },
    {
        id: 2,
        img: 'https://i.pinimg.com/736x/84/ba/64/84ba64190cafb91bad0314858dadd44f.jpg',
        title: 'BAGS COLLECTION',
        desc: 'GET 30% OFF FOR NEW ARRIVALS ',
        bg: 'fcf1ed',
    },
    {
        id: 3,
        img: 'https://i.pinimg.com/736x/84/ba/64/84ba64190cafb91bad0314858dadd44f.jpg',
        title: 'COSMETIC COLLECTION',
        desc: 'GET 30% OFF FOR NEW ARRIVALS ',
        bg: 'ddedea',
    },
];

export const categories = [
    {
        id: 1,
        img: 'https://images.pexels.com/photos/5321675/pexels-photo-5321675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        title: 'SUMMER FASHION',
    },
    {
        id: 2,
        img: 'https://images.pexels.com/photos/2043590/pexels-photo-2043590.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        title: 'BAGS',
    },
    {
        id: 3,
        img: 'https://images.pexels.com/photos/2639947/pexels-photo-2639947.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        title: 'MAKEUP',
    },
];

export const productsItem = [
    {
        id: 1,
        img: 'https://images.pexels.com/photos/3661622/pexels-photo-3661622.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
        id: 2,
        img: 'https://images.pexels.com/photos/2688991/pexels-photo-2688991.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',  
    },
    {
        id: 3,
        img: 'https://images.pexels.com/photos/6310924/pexels-photo-6310924.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
        id: 4,
        img: 'https://images.pexels.com/photos/10565426/pexels-photo-10565426.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
        id: 5,
        img: 'https://images.pexels.com/photos/6310184/pexels-photo-6310184.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
        id: 6,
        img: 'https://images.pexels.com/photos/6454667/pexels-photo-6454667.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
        id: 7,
        img: 'https://images.pexels.com/photos/6311591/pexels-photo-6311591.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
    {
        id: 8,
        img: 'https://images.pexels.com/photos/2637820/pexels-photo-2637820.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
    },
];
