import React from 'react';
import styled from 'styled-components';
import { Badge } from '@material-ui/core';
import { ShoppingCartOutlined } from '@material-ui/icons';
import { Link } from 'react-router-dom'
import { mobile } from '../responsive'

const Container = styled.div`
    height: 60px;
    ${mobile({ height: '50px'})}
`;
const Wrapper = styled.div`
    margin-top: 10px;
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    ${mobile({ height: '10px 0px'})}
`;
const Left = styled.div`
    flex:1;
`;
const Logo = styled.h1`
    font-weight: bold;
    font-size: 20px;
    cursor: pointer; 
    ${mobile({ fontSize: '24px'})}   
`;

const Right = styled.div`
    flex:1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    ${mobile({ flex:2, justifyContent: 'center'})} 
`;

const MenuItem = styled.div`
    font-size: 14px;
    cursor: pointer;
    margin-left: 25px;
    ${mobile({ fontSize: '12px', marginLeft: '10px'})} 
` 

function Navbar() {
    
    return (
        <Container>
            <Wrapper>
                <Left>
                    <Logo>OnlineShop.</Logo>
                </Left>
                <Right>
                    <MenuItem><Link to="/Register" style={{ textDecoration: 'none' }}>REGISTER</Link></MenuItem>
                    <MenuItem><Link to="/login" style={{ textDecoration: 'none' }}>SIGN IN</Link></MenuItem>
                    <MenuItem>
                    <Badge color="secondary" badgeContent={4}>
                        <ShoppingCartOutlined />
                    </Badge>
                    </MenuItem>
                </Right>
            </Wrapper>
        </Container>
    )
}

export default Navbar
