import React from "react";
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Cart from "./pages/Cart";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';

const App = () =>  {

  return (
    <Router>
      <Container>
      <Routes>
      <Route exact path="/" element={<Home/>}/> 
      <Route path="/cart" element={<Cart/>}/>
      <Route path="/login" element={<Login/>}/>
      <Route path="/register" element={<Register/>}/> 
      
      </Routes>
      </Container>
    </Router>
  );
};

export default App;
