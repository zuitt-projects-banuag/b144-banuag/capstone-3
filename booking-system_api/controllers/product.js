const Course = require("../models/Course");

// Creating a course
module.exports.addProduct = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProdcut = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		// Saves the created object to our database
		return newProdcut.save().then((product, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};

module.exports.addProduct = (reqBody, userData) => {

    return Product.findById(userData.userId).then(result => {

        if (userData.isAdmin) {
            return "You are not an admin"
        } else {
            let newProdcut = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newProdcut.save().then((product, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}

// Retrieve all courses
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
};

// // Retrieve specific course
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a course
module.exports.archiveProduct = (reqParams) => {
	let updatedActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedActiveField).then((product, error) => {

		if (error){
			return false;
		}
		else{
			return true;
		}
	})
}