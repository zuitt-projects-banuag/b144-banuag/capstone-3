const mongoose = require('mongoose');

const CartSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "ID name is required"],
	},
	products: [
        {
            productId: {
                type: String,
            },
            quantity: {
                type: Number,
                default: 1,
            },
        },
    ],
});

module.exports = mongoose.model('Cart', CartSchema);