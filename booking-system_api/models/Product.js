const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({

	title: {
		type: String,
		required: [true, "Product name is required"],
		unique: true
	},
	desc: {
		type: String,
		required: [true, "Description is required"]
	},
	img: {
		type: String,
		required: [true, "Image is required"]
	},
	categories: {
		type: Array
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	inStock: {
		type: Boolean,
		default: true
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
});

module.exports = mongoose.model('Product', ProductSchema);